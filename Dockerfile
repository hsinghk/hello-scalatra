
FROM tomcat
MAINTAINER qatrainer

COPY target/hello-scalatra.war /usr/local/tomcat/webapps/hello-scalatra.war

EXPOSE 8080

CMD ["catalina.sh", "run"]
